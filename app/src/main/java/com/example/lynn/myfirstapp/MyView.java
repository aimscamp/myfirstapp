package com.example.lynn.myfirstapp;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.lynn.myfirstapp.MainActivity.*;

/**
 * Created by lynn on 6/13/2016.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        Drawable background = getResources().getDrawable(R.drawable.mybackground);

        setBackground(background);

        button = new Button(context);

        button.setText("Press Me");

        button.setTag("Red;Blue");

        button.setOnClickListener(listener);

        addView(button);
    }

}
